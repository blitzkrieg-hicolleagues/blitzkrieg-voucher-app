package controller

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"blitzkrieg-voucher-app/pkg/dto"
	"blitzkrieg-voucher-app/shared/util"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

// tipe struct yang mengendalikan HTTP handler terkait entitas student
type WalletsController struct {
	WalletsUsecase domain.WalletsUsecase
}

// CreateWallets, method yang membuat data Wallets baru berdasarkan data yang diberikan dalam request bodyy
func (wc *WalletsController) CreateWallets(c echo.Context) error {
	var request dto.WalletsDTO
	if err := c.Bind(&request); err != nil {
		return util.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}

	if err := request.Validation(); err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	if err := wc.WalletsUsecase.CreateWallets(request); err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return util.SetResponse(c, http.StatusOK, "success added wallet", nil)
}

func (wc *WalletsController) GetWallets(c echo.Context) error {
	resp, err := wc.WalletsUsecase.GetWallets()
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return util.SetResponse(c, http.StatusOK, "success view all wallet", resp)
}

func (wc *WalletsController) GetWalletsById(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	resp, err := wc.WalletsUsecase.GetWalletsById(id)

	if err != nil {
		return util.SetResponse(c, http.StatusNotFound, "id Wallet not found", nil)
	}
	return util.SetResponse(c, http.StatusOK, "success search Wallet by id", resp)
}

func (wc *WalletsController) UpdateWallets(c echo.Context) error {
	var request dto.WalletsDTO

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}

	_, err = wc.WalletsUsecase.GetWalletsById(id)
	if err != nil {
		return util.SetResponse(c, http.StatusNotFound, "failed to update, user not found", nil)
	}

	if err := c.Bind(&request); err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	if err := request.Validation(); err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	if err := wc.WalletsUsecase.UpdateWallets(id, request); err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return util.SetResponse(c, http.StatusOK, "success update wallet", nil)
}
