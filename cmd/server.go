package cmd

import (
	"blitzkrieg-voucher-app/config"
	"blitzkrieg-voucher-app/pkg/router"
	"blitzkrieg-voucher-app/shared/db"
	"fmt"

	"github.com/golang-jwt/jwt/v5"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func RunServer() {
	e := echo.New()
	g := e.Group("api")
	conf := config.GetConfig()
	g.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		TokenLookup: "header:Authorization",
		AuthScheme:  "Bearer",
		ParseTokenFunc: func(auth string, c echo.Context) (interface{}, error) {
			keyFunc := func(t *jwt.Token) (interface{}, error) {
				if t.Method.Alg() != "HS512" {
					return nil, fmt.Errorf("jwt token is formatted incorrectly")
				}
				return []byte(conf.SignKey), nil
			}
			token, err := jwt.Parse(auth, keyFunc)
			if err != nil {
				return nil, err
			}
			if !token.Valid {
				return nil, err
			}
			return token, err
		},
	}))

	Apply(e, g, conf)

	e.Logger.Error(e.Start(":8080"))
}

func Apply(e *echo.Echo, g *echo.Group, conf config.Configuration) {
	db := db.NewInstanceDb(conf)
	gc := g.Group("/counter")
	gu := g.Group("/user")
	ge := g.Group("/paket-data")
	gw := g.Group("/wallet")

	router.NewUserRouter(e, gu, db)
	router.NewWalletsRouter(e, gw, db)
	router.NewListPaketDataRouter(e, gc, db)
	router.NewPaketDataRouter(e, ge, db)
}
