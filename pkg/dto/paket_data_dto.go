package dto

import validation "github.com/go-ozzo/ozzo-validation"

type ChangePaketDataRequest struct {
	UserId int `json:"user_id"`
	Type   int `json:"type"`
	Kuota  int `json:"kuota"`
}

func (cpd ChangePaketDataRequest) Validation() error {
	// menggunakan package "validation" untuk melakukan validasi terstruktur pada field dalam UserDTO
	// validasi ini memastikan bahwa data yang diterima dari permintaan HTTP memenuhi kriteria yang diharapkan
	err := validation.ValidateStruct(&cpd,
		validation.Field(&cpd.UserId, validation.Required),
		validation.Field(&cpd.Type, validation.Required),
		validation.Field(&cpd.Kuota, validation.Required),
	)
	return err
}
