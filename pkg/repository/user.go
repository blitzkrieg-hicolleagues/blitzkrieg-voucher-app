package repository

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"database/sql"
)

// tipe yang bertanggung jawab atas interaksi dengan database terkait data user
type UserRepository struct {
	db *sql.DB // instance dari koneksi databases
}

// NewUserRepository, fungsi pembuat untuk membuat instance baru dari UserRepository
func NewUserRepository(db *sql.DB) domain.UserRepository {
	return &UserRepository{
		db: db,
	}
}

// CreateUser, metode untuk membuat data user baru dalam database
func (ur UserRepository) CreateUser(req domain.User) error {
	sql := `INSERT INTO users (email, username, age, password) values ($1, $2, $3, $4)`
	_, err2 := ur.db.Exec(sql, req.Email, req.Username, req.Age, req.Password)
	if err2 != nil {
		return err2
	}
	return nil
}

// GetUser, metode yang mengambil daftar user dari database
func (ur UserRepository) GetUsers() ([]domain.UserView, error) {
	sql := `SELECT id, email, username, age, created_at FROM users ORDER BY id ASC`
	rows, err := ur.db.Query(sql)
	if err != nil {
		return nil, err
	}
	var users []domain.UserView
	for rows.Next() {
		user := domain.UserView{}
		err2 := rows.Scan(&user.Id, &user.Email, &user.Username, &user.Age, &user.CreatedAt)
		if err2 != nil {
			return users, err2
		}
		users = append(users, user)
	}
	return users, err
}

// GetUser, metode yang mengambil data user berdasarkan ID dari database
func (ur UserRepository) GetUserById(id int) (domain.UserView, error) {
	var user domain.UserView
	sql := `SELECT id, email, username, age, created_at FROM users WHERE id = $1`
	err := ur.db.QueryRow(sql, id).Scan(&user.Id, &user.Email, &user.Username, &user.Age, &user.CreatedAt)
	return user, err
}

// FindByEmail, metode yang mencari daftar user berdasarkan email dari database
func (ur UserRepository) FindByEmail(email string) (domain.User, error) {
	var user domain.User
	sql := `select * from users where email = $1`
	err := ur.db.QueryRow(sql, email).Scan(&user.Id, &user.Email, &user.Username, &user.Age, &user.Password, &user.CreatedAt)
	return user, err
}

// FindByUsername, metode yang mencari daftar user berdasarkan username dari database
func (ur UserRepository) FindByUsername(username string) (domain.User, error) {
	var user domain.User
	sql := `select * from users where username = $1`
	err := ur.db.QueryRow(sql, username).Scan(&user.Id, &user.Email, &user.Username, &user.Age, &user.Password, &user.CreatedAt)
	return user, err
}

func (ur UserRepository) UpdateUser(id int, req domain.User) error {
	sql := `UPDATE users SET email = $1, username = $2, age = $3, password = $4 WHERE id = $5`
	_, err2 := ur.db.Exec(sql, req.Email, req.Username, req.Age, req.Password, id)
	if err2 != nil {
		return err2
	}
	return nil
}

// DeleteUser, metode untuk menghapus data user dari database
func (ur UserRepository) DeleteUserById(id int) error {
	sql := `DELETE FROM users WHERE id = $1`
	_, err2 := ur.db.Exec(sql, id)
	if err2 != nil {
		return err2
	}
	return nil
}
