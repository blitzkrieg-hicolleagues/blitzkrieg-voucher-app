package domain

import "blitzkrieg-voucher-app/pkg/dto"

type PaketData struct {
	Id     int `json:"id"`
	UserId int `json:"user_id"`
	Type   int `json:"type"`
	Kuota  int `json:"kuota"`
}

type PaketDataRepository interface {
	GetPaketData(paketDataId int) (PaketData, error)
	// ResetPaketData(paketDataId int) error
	CreatePaketData(paketData PaketData) error
	UpdatePaketData(paketData PaketData) error
}
type PaketDataUsecase interface {
	GetPaketData(paketDataId int) (PaketData, error)
	// ResetPaketData(paketDataId int) error
	CreatePaketData(paketData PaketData) error
	UpdatePaketData(req dto.ChangePaketDataRequest) error
}
