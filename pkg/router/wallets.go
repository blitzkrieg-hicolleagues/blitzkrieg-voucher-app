package router

import (
	"blitzkrieg-voucher-app/pkg/controller"
	"blitzkrieg-voucher-app/pkg/repository"
	"blitzkrieg-voucher-app/pkg/usecase"
	"database/sql"

	"github.com/labstack/echo/v4"
)

// NewWalletsRouter, fungsi untuk mengkonfigurasikan rute-rute terkait student dalam Echo framework
func NewWalletsRouter(e *echo.Echo, gw *echo.Group, db *sql.DB) {
	// membuat instance dari WalletsRepository yang berinteraksi dengan database
	wr := repository.NewWalletsRepository(db)
	// membuat instance dari WalletsUsecase yang berfungsi sebagai perantara antara repository dan controller
	wu := usecase.NewWalletsUsecase(wr)
	// membuat instance dari WalletsController yang mengatur logika untuk endpoint
	wc := &controller.WalletsController{
		WalletsUsecase: wu,
	}

	// Mengatur rute HTTP menggunakan Echo untuk mendefinisikan endpoint
	gw.POST("", wc.CreateWallets)     // POST /user, memanggil fungsi CreateWallets di WalletsController untuk membuat data baru
	gw.GET("", wc.GetWallets)         // GET /user, memanggil fungsi GetWalletss di WalletsController
	gw.GET("/:id", wc.GetWalletsById) // GET /user/:id, memanggil fungsi GetWallets di WalletsController dengan parameter ID
	gw.PUT("/:id", wc.UpdateWallets)  // PUT /user/:id, memanggil fungsi UpdateWallets di WalletsController dengan parameter ID
}
