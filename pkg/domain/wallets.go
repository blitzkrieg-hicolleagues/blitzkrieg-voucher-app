package domain

import "blitzkrieg-voucher-app/pkg/dto"

type Wallets struct {
	Id        int     `json:"id"`
	Saldo     float64 `json:"saldo"`
	UserId    int     `json:"user_id"`
	CreatedAt string  `json:"created_ad"`
}

type WalletsRepository interface {
	CreateWallets(req Wallets) error
	FindWalletByUserId(user_id int) (Wallets, error)
	UpdateWallets(id int, req Wallets) error
	GetWallets() ([]Wallets, error)
	GetWalletsById(id int) (Wallets, error)
}

type WalletsUsecase interface {
	CreateWallets(req dto.WalletsDTO) error
	UpdateWallets(id int, req dto.WalletsDTO) error
	GetWallets() ([]Wallets, error)
	GetWalletsById(id int) (Wallets, error)
	FindWalletByUserId(user_id int) (Wallets, error)
}
