package usecase

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"blitzkrieg-voucher-app/pkg/dto"

	"github.com/mitchellh/mapstructure"
)

type PaketDataUsecase struct {
	repo domain.PaketDataRepository
}

func NewPaketDataUsecase(repo domain.PaketDataRepository) domain.PaketDataUsecase {
	return &PaketDataUsecase{
		repo: repo,
	}
}

func (usecase *PaketDataUsecase) GetPaketData(id int) (domain.PaketData, error) {
	return usecase.repo.GetPaketData(id)
}

// func (usecase *PaketDataUsecase) ResetPaketData(id int) error {
// 	return usecase.repo.ResetPaketData(id)
// }

func (usecase *PaketDataUsecase) CreatePaketData(req domain.PaketData) error {
	return usecase.repo.CreatePaketData(req)
}

func (usecase *PaketDataUsecase) UpdatePaketData(req dto.ChangePaketDataRequest) error {
	err := req.Validation()
	if err != nil {
		return err
	}

	var paketData domain.PaketData
	mapstructure.Decode(req, &paketData)

	return usecase.repo.UpdatePaketData(paketData)
}
