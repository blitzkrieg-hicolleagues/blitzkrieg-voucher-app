package dto

import validation "github.com/go-ozzo/ozzo-validation"

type ListPaketDataDTO struct {
	Type  int     `json:"type"`
	Kuota int     `json:"kuota"`
	Price float64 `json:"price"`
	Stock int     `json:"stock"`
}

type BeliPaketData struct {
	UserId          int `json:"user_id"`
	ListPaketDataId int `json:"list_paket_data_id"`
}

func (lpd ListPaketDataDTO) Validation() error {
	err := validation.ValidateStruct(&lpd,
		validation.Field(&lpd.Type, validation.Required),
		validation.Field(&lpd.Kuota, validation.Required),
		validation.Field(&lpd.Price, validation.Required),
		validation.Field(&lpd.Stock, validation.Required),
	)

	return err
}

func (lpd BeliPaketData) ValidationBeliPaketData() error {
	err := validation.ValidateStruct(&lpd,
		validation.Field(&lpd.UserId, validation.Required),
		validation.Field(&lpd.ListPaketDataId, validation.Required),
	)

	return err
}
