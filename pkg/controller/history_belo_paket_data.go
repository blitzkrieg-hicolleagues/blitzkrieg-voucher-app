package controller

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"blitzkrieg-voucher-app/shared/util"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

type HistoryBeliPaketDataController struct {
	HistoryBeliPaketDataUsecase domain.HistoryBeliPaketDataUsecase
}

func (hbpdc HistoryBeliPaketDataController) GetHistoryBeliPaketData(c echo.Context) error {
	res, err := hbpdc.HistoryBeliPaketDataUsecase.GeHistoryBeliPaketData()
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success fetch all history beli paket data", res)
}

func (hbpdc HistoryBeliPaketDataController) GetHistoryBeliPaketDataById(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	res, err := hbpdc.HistoryBeliPaketDataUsecase.GetHistoryBeliPaketDataById(id)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success fetch history beli paket data", res)
}

// func (hbpdc HistoryBeliPaketDataController) CreateHistoryBeliPaketData(c echo.Context) error {
// 	// Get list paket data ID
// 	// Get user ID
// 	// Get paket data ID
// 	// Get wallet ID
// 	res, err := hbpdc.HistoryBeliPaketDataUsecase.GeHistoryBeliPaketData()
// 	if err != nil {
// 		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
// 	}

// 	return util.SetResponse(c, http.StatusOK, "success fetch history beli paket data", res)
// }

func (hbpdc HistoryBeliPaketDataController) DeleteHistoryBeliPaketData(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = hbpdc.HistoryBeliPaketDataUsecase.DeleteHistoryBeliPaketData(id)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success delete history beli paket data", nil)
}
