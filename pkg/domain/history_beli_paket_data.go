package domain

import (
	"blitzkrieg-voucher-app/pkg/dto"
	"time"
)

type HistoryBeliPaketData struct {
	Id              int       `json:"id"`
	ListPaketDataId int       `json:"list_paket_data_id"`
	UserId          int       `json:"user_id"`
	PaketDataId     int       `json:"paket_data_id"`
	WalletId        int       `json:"wallet_id"`
	CreatedAt       time.Time `json:"created_at"`
}

type HistoryBeliPaketDataRepository interface {
	GeHistoryBeliPaketData() ([]HistoryBeliPaketData, error)
	GetHistoryBeliPaketDataById(id int) (HistoryBeliPaketData, error)
	CreateHistoryBeliPaketData(req HistoryBeliPaketData) error
	// UpdateHistoryBeliPaketData(id int, req HistoryBeliPaketData) error
	DeleteHistoryBeliPaketData(id int) error
}

type HistoryBeliPaketDataUsecase interface {
	GeHistoryBeliPaketData() ([]HistoryBeliPaketData, error)
	GetHistoryBeliPaketDataById(id int) (HistoryBeliPaketData, error)
	CreateHistoryBeliPaketData(req dto.HistoryBeliPaketDataDTO) error
	// UpdateHistoryBeliPaketData(id int, req dto.HistoryBeliPaketDataDTO) error
	DeleteHistoryBeliPaketData(id int) error
}
