package router

import (
	"blitzkrieg-voucher-app/pkg/controller"
	"blitzkrieg-voucher-app/pkg/repository"
	"blitzkrieg-voucher-app/pkg/usecase"
	"database/sql"

	"github.com/labstack/echo/v4"
)

func NewListPaketDataRouter(e *echo.Echo, gc *echo.Group, db *sql.DB) {
	lpdr := repository.NewListPaketDataRepository(db)
	pdr := repository.NewPaketDataRepository(db)
	wr := repository.NewWalletsRepository(db)
	hbpdr := repository.NewHistoryBeliPaketDataRepository(db)
	lpdu := usecase.NewListPaketDataUsecase(lpdr, pdr, wr, hbpdr)
	fhc := &controller.ListPaketDataController{
		ListPaketDataUsecase: lpdu,
	}

	gc.GET("/stock", fhc.GetListPaketData)
	gc.POST("/stock", fhc.CreateListPaketData)
	gc.PUT("/stock/:id", fhc.UpdateListPaketData)
	gc.DELETE("/stock/:id", fhc.DeleteListPaketData)
	gc.POST("/buy", fhc.BeliPaketData)
}
