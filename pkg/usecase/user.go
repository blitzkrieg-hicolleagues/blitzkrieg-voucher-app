package usecase

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"blitzkrieg-voucher-app/pkg/dto"
	"blitzkrieg-voucher-app/shared/util"
	"errors"
	"fmt"

	"github.com/mitchellh/mapstructure"
)

// tipe yang bertanggung jawab atas operasi-operasi terkait user.
type UserUsecase struct {
	UserRepository      domain.UserRepository // Mengandung dependency ke repository untuk mengakses data user
	PaketDataRepository domain.PaketDataRepository
	WalletsRepository   domain.WalletsRepository
}

// NewUserUsecase, fungsi pembuat untuk membuat instance baru dari UserUsecase
func NewUserUsecase(userRepository domain.UserRepository, paketDataRepository domain.PaketDataRepository, walletsRepository domain.WalletsRepository) domain.UserUsecase {
	return &UserUsecase{
		UserRepository:      userRepository,
		PaketDataRepository: paketDataRepository,
		WalletsRepository:   walletsRepository,
	}
}

// CreateUser, metode untuk membuat data user baru
func (uu UserUsecase) CreateUser(req dto.UserDTO) error {

	var user domain.User
	mapstructure.Decode(req, &user)

	// memeriksa apakah email sudah terdaftar
	if _, err := uu.UserRepository.FindByEmail(user.Email); err == nil {
		return errors.New("email already exists")
	}

	// memeriksa apakah username sudah terdaftar
	if _, err := uu.UserRepository.FindByUsername(user.Username); err == nil {
		return errors.New("username already exists")
	}

	// enkripsi kata sandi
	hashedPassword, err := util.EncryptPassword(user.Password)
	if err != nil {
		return err
	}
	user.Password = hashedPassword

	// membuat pengguna baru
	err = uu.UserRepository.CreateUser(user)
	if err != nil {
		return err
	}

	userByEmail, err := uu.UserRepository.FindByEmail(user.Email)
	if err != nil {
		return err
	}
	fmt.Println(userByEmail)

	userPaketData := domain.PaketData{
		UserId: userByEmail.Id,
		Type:   0,
		Kuota:  0,
	}

	err = uu.PaketDataRepository.CreatePaketData(userPaketData)
	if err != nil {
		return err
	}

	userWallet := domain.Wallets{
		Saldo:  0,
		UserId: userByEmail.Id,
	}

	err = uu.WalletsRepository.CreateWallets(userWallet)
	if err != nil {
		return err
	}

	return nil
}

// GetUsers, metode yang mengambil daftar user dari database
func (uu UserUsecase) GetUsers() ([]domain.UserView, error) {
	// memanggil metode GetUsers dari UserRepository untuk mengambil data dari database
	return uu.UserRepository.GetUsers()
}

// GetUser, metode yang mengambil data user berdasarkan ID
func (uu UserUsecase) GetUserById(id int) (domain.UserView, error) {
	// memanggil metode GetUser dari UserRepository untuk mengambil data dari database
	return uu.UserRepository.GetUserById(id)
}

func (uu UserUsecase) UserLogin(req dto.LoginRequest) (interface{}, error) {
	var loginResponse dto.LoginResponse

	// memeriksa pengguna berdasarkan alamat email
	user, err := uu.UserRepository.FindByEmail(req.Email)
	if err != nil {
		return nil, errors.New("email not found")
	}

	// verifikasi kata sandi
	valid, err := util.VerifyPassword(req.Password, user.Password)
	if err != nil {
		return nil, errors.New("password not valid")
	}

	// jika kata sandi tidak valid, kembalikan pesan kesalahan
	if !valid {
		return nil, errors.New("bad credential")
	}

	// membuat token JWT
	token, err := util.CreateJwtToken(user)
	if err != nil {
		return nil, err
	}

	// respons dengan data pengguna dan token
	mapstructure.Decode(user, &loginResponse)
	loginResponse.Token = token

	return loginResponse, nil
}

// UpdateUser, metode untuk memperbarui data user
func (uu UserUsecase) UpdateUser(id int, req dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(req, &user)

	// Jika kata sandi baru diberikan, enkripsi kata sandi tersebut
	if req.Password != "" {
		encryptedPassword, err := util.EncryptPassword(req.Password)
		if err != nil {
			// Tangani kesalahan jika terjadi
			return err
		}
		user.Password = encryptedPassword
	}

	return uu.UserRepository.UpdateUser(id, user)
}

// DeleteUser, metode untuk menghapus data user
func (uu UserUsecase) DeleteUserById(id int) error {
	// memanggil metode DeleteUser dari UserRepository untuk menghapus data dari database
	return uu.UserRepository.DeleteUserById(id)
}
