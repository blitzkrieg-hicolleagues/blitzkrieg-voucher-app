package dto

import validation "github.com/go-ozzo/ozzo-validation"

type WalletsDTO struct {
	Saldo     float64 `json:"saldo"`
	UserId    int     `json:"user_id"`
	CreatedAt string  `json:"created_ad"`
}

func (w WalletsDTO) Validation() error {
	// menggunakan package "validation" untuk melakukan validasi terstruktur pada field dalam WalletsDTO
	// validasi ini memastikan bahwa data yang diterima dari permintaan HTTP memenuhi kriteria yang diharapkan
	err := validation.ValidateStruct(&w,
		validation.Field(&w.Saldo, validation.Required),
		validation.Field(&w.UserId, validation.Required))

	if err != nil {
		return err
	}
	return nil
}
