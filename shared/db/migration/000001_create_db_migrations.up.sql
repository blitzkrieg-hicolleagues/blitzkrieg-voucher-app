CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    email VARCHAR(255) UNIQUE NOT NULL,
    username VARCHAR(100) UNIQUE NOT NULL,
    age INT,
    password VARCHAR(1000) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE list_paket_data (
    id BIGSERIAL PRIMARY KEY,
    type INT,
    kuota INT,
    price FLOAT,
    stock INT
);

CREATE TABLE wallets (
    id BIGSERIAL PRIMARY KEY,
    saldo FLOAT DEFAULT 0,
    user_id BIGINT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE paket_data (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGINT,
    type INT DEFAULT 0,
    kuota INT DEFAULT 0,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE history_beli_paket_data (
    id BIGSERIAL PRIMARY KEY,
    list_paket_data_id BIGINT,
    user_id BIGINT,
    paket_data_id BIGINT,
    wallet_id BIGINT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (list_paket_data_id) REFERENCES list_paket_data(id),
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (paket_data_id) REFERENCES paket_data(id),
    FOREIGN KEY (wallet_id) REFERENCES wallets(id)
);
