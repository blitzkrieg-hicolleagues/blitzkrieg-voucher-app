package usecase

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"blitzkrieg-voucher-app/pkg/dto"

	"github.com/mitchellh/mapstructure"
)

type HistoryBeliPaketDataUsecase struct {
	HistoryBeliPaketDataRepository domain.HistoryBeliPaketDataRepository
}

func NewHistoryBeliPaketDataUsecase(historyBeliPaketDataRepository domain.HistoryBeliPaketDataRepository) domain.HistoryBeliPaketDataUsecase {
	return &HistoryBeliPaketDataUsecase{historyBeliPaketDataRepository}
}

func (hbpdu HistoryBeliPaketDataUsecase) GeHistoryBeliPaketData() ([]domain.HistoryBeliPaketData, error) {
	return hbpdu.HistoryBeliPaketDataRepository.GeHistoryBeliPaketData()
}

func (hbpdu HistoryBeliPaketDataUsecase) GetHistoryBeliPaketDataById(id int) (domain.HistoryBeliPaketData, error) {
	return hbpdu.HistoryBeliPaketDataRepository.GetHistoryBeliPaketDataById(id)
}

func (hbpdu HistoryBeliPaketDataUsecase) CreateHistoryBeliPaketData(req dto.HistoryBeliPaketDataDTO) error {
	var historyBeli domain.HistoryBeliPaketData
	mapstructure.Decode(req, &historyBeli)
	return hbpdu.HistoryBeliPaketDataRepository.CreateHistoryBeliPaketData(historyBeli)
}

func (hbpdu HistoryBeliPaketDataUsecase) DeleteHistoryBeliPaketData(id int) error {
	return hbpdu.HistoryBeliPaketDataRepository.DeleteHistoryBeliPaketData(id)
}
