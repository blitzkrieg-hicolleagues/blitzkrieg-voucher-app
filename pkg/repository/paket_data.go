package repository

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"database/sql"
	"errors"
	"fmt"
)

type PaketDataRepository struct {
	db *sql.DB
}

func NewPaketDataRepository(db *sql.DB) domain.PaketDataRepository {
	return &PaketDataRepository{db: db}
}

func (repo *PaketDataRepository) GetPaketData(id int) (domain.PaketData, error) {
	const query = "SELECT id, user_id, type, kuota FROM paket_data WHERE user_id = $1"

	var paketData domain.PaketData
	err := repo.db.QueryRow(query, id).Scan(&paketData.Id, &paketData.UserId, &paketData.Type, &paketData.Kuota)
	if err != nil {
		if err == sql.ErrNoRows {
			return domain.PaketData{}, errors.New("paket data not found")
		}
		return domain.PaketData{}, fmt.Errorf("database error: %v", err)
	}

	return paketData, nil
}

// func (repo *PaketDataRepository) ResetPaketData(id int) error {
// 	const query = "UPDATE paket_data SET type = 0, kuota = 0 WHERE id = $1"

// 	_, err := repo.db.Exec(query, id)
// 	return err
// }

func (repo *PaketDataRepository) CreatePaketData(paketData domain.PaketData) (err error) {
	sqlStatement := `INSERT INTO paket_data (user_id, type, kuota) VALUES ($1, $2, $3)`

	stmt, err := repo.db.Prepare(sqlStatement)
	if err != nil {
		return err
	}
	defer func() {
		closeErr := stmt.Close()
		if err == nil && closeErr != nil {
			err = closeErr
		}
	}()

	_, err = stmt.Exec(paketData.UserId, paketData.Type, paketData.Kuota)
	if err != nil {
		return err
	}

	return nil
}

func (repo *PaketDataRepository) UpdatePaketData(req domain.PaketData) error {
	const query = "UPDATE paket_data SET type = $2, kuota = $3 WHERE user_id = $1"

	_, err := repo.db.Exec(query, req.UserId, req.Type, req.Kuota)
	return err
}
