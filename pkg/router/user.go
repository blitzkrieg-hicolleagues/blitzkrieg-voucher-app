package router

import (
	"blitzkrieg-voucher-app/pkg/controller"
	"blitzkrieg-voucher-app/pkg/repository"
	"blitzkrieg-voucher-app/pkg/usecase"
	"database/sql"

	"github.com/labstack/echo/v4"
)

// NewUserRouter, fungsi untuk mengkonfigurasikan rute-rute terkait student dalam Echo framework
func NewUserRouter(e *echo.Echo, gu *echo.Group, db *sql.DB) {
	// membuat instance dari UserRepository yang berinteraksi dengan database
	ur := repository.NewUserRepository(db)
	pdr := repository.NewPaketDataRepository(db)
	wr := repository.NewWalletsRepository(db)
	// membuat instance dari UserUsecase yang berfungsi sebagai perantara antara repository dan controller
	uu := usecase.NewUserUsecase(ur, pdr, wr)
	// membuat instance dari UserController yang mengatur logika untuk endpoint
	uc := &controller.UserController{
		UserUsecase: uu,
	}

	// Mengatur rute HTTP menggunakan Echo untuk mendefinisikan endpoint
	e.POST("/api/register", uc.CreateUser) // POST /user, memanggil fungsi CreateUser di UserController untuk membuat data baru
	e.POST("/api/login", uc.Login)
	gu.GET("", uc.GetUsers)          // GET /user, memanggil fungsi GetUsers di UserController
	gu.GET("/:id", uc.GetUserById)   // GET /user/:id, memanggil fungsi GetUser di UserController dengan parameter ID
	gu.PUT("/:id", uc.UpdateUser)    // PUT /user/:id, memanggil fungsi UpdateUser di UserController dengan parameter ID
	gu.DELETE("/:id", uc.DeleteUser) // DELETE /user/:id, memanggil fungsi DeleteUser di UserController dengan parameter ID
}
