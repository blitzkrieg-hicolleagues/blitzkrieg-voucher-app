package usecase

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"blitzkrieg-voucher-app/pkg/dto"

	"github.com/mitchellh/mapstructure"
)

// tipe yang bertanggung jawab atas operasi-operasi terkait user.
type WalletsUsecase struct {
	WalletsRepository domain.WalletsRepository // Mengandung dependency ke repository untuk mengakses data user
}

// NewWalletsUsecase, fungsi pembuat untuk membuat instance baru dari WalletsUsecase
func NewWalletsUsecase(walletsRepository domain.WalletsRepository) domain.WalletsUsecase {
	return &WalletsUsecase{
		WalletsRepository: walletsRepository,
	}
}

// CreateWallets, metode untuk membuat data user baru
func (wu WalletsUsecase) CreateWallets(req dto.WalletsDTO) error {
	var wallets domain.Wallets
	if err := mapstructure.Decode(req, &wallets); err != nil {
		return err
	}
	return wu.WalletsRepository.CreateWallets(wallets)
}

func (wu WalletsUsecase) GetWallets() ([]domain.Wallets, error) {
	// memanggil metode GetWallets dari UserRepository untuk mengambil data dari database
	return wu.WalletsRepository.GetWallets()
}

func (wu WalletsUsecase) GetWalletsById(id int) (domain.Wallets, error) {
	// memanggil metode GetUser dari UserRepository untuk mengambil data dari database
	return wu.WalletsRepository.GetWalletsById(id)
}

func (wu WalletsUsecase) UpdateWallets(id int, req dto.WalletsDTO) error {
	var wallets domain.Wallets
	if err := mapstructure.Decode(req, &wallets); err != nil {
		return err
	}
	return wu.WalletsRepository.UpdateWallets(id, wallets)
}

func (wu WalletsUsecase) FindWalletByUserId(user_id int) (domain.Wallets, error) {
	// memanggil metode GetUser dari UserRepository untuk mengambil data dari database
	return wu.WalletsRepository.FindWalletByUserId(user_id)
}
