package main

import (
	"blitzkrieg-voucher-app/cmd"
)

func main() {
	cmd.RunServer()
}
