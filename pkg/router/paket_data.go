package router

import (
	"blitzkrieg-voucher-app/pkg/controller"
	"blitzkrieg-voucher-app/pkg/repository"
	"blitzkrieg-voucher-app/pkg/usecase"
	"database/sql"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// NewPaketDataRouter initializes new router for PaketData
func NewPaketDataRouter(e *echo.Echo, ge *echo.Group, db *sql.DB) {
	// Initialize dependencies
	repo := repository.NewPaketDataRepository(db)
	usecaseInstance := usecase.NewPaketDataUsecase(repo)
	ctrl := controller.PaketDataController{Usecase: usecaseInstance}

	// Use middleware if needed
	ge.Use(middleware.Logger())
	
	// Routes
	// Retrieve PaketData by ID
	ge.GET("/:id", ctrl.GetPaketData) 
	
	// Reset PaketData by ID
	// ge.PUT("/:id", ctrl.ResetPaketData)
}
