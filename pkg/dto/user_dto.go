package dto

import validation "github.com/go-ozzo/ozzo-validation"

type UserDTO struct {
	Email     string `json:"email"`
	Age       string `json:"age"`
	Password  string `json:"password"`
	CreatedAt string `json:"created_ad"`
	Username  string `json:"username"`
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Id        int    `json:"id"`
	Email     string `json:"email"`
	Age       string `json:"age"`
	CreatedAt string `json:"created_ad"`
	Username  string `json:"username"`
	Token     string `json:"token"`
}

// Validation, method yang digunakan untuk melakukan validasi pada data yang ada dalam UserDTO
func (u UserDTO) Validation() error {
	// menggunakan package "validation" untuk melakukan validasi terstruktur pada field dalam UserDTO
	// validasi ini memastikan bahwa data yang diterima dari permintaan HTTP memenuhi kriteria yang diharapkan
	err := validation.ValidateStruct(&u,
		validation.Field(&u.Email, validation.Required),
		validation.Field(&u.Age, validation.Required),
		validation.Field(&u.Password, validation.Required),
		validation.Field(&u.Username, validation.Required))

	if err != nil {
		return err
	}
	return nil
}
