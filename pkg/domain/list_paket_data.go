package domain

import "blitzkrieg-voucher-app/pkg/dto"

type ListPaketData struct {
	Id    int     `json:"id"`
	Type  int     `json:"type"`
	Kuota int     `json:"kuota"`
	Price float64 `json:"price"`
	Stock int     `json:"stock"`
}

type ListPaketDataRepository interface {
	GetListPaketData() ([]ListPaketData, error)
	GetListPaketDataById(id int) (ListPaketData, error)
	CreateListPaketData(req ListPaketData) error
	UpdateListPaketData(id int, req ListPaketData) error
	UpdateListPaketDataStock(id, stock int) error
	DeleteListPaketData(id int) error
}

type ListPaketDataUsecase interface {
	GetListPaketData() ([]ListPaketData, error)
	GetListPaketDataById(id int) (ListPaketData, error)
	CreateListPaketData(req dto.ListPaketDataDTO) error
	UpdateListPaketData(id int, req dto.ListPaketDataDTO) error
	DeleteListPaketData(id int) error
	BeliPaketData(req dto.BeliPaketData) error
}
