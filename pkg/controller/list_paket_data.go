package controller

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"blitzkrieg-voucher-app/pkg/dto"
	"net/http"
	"strconv"

	"blitzkrieg-voucher-app/shared/util"

	"github.com/labstack/echo/v4"
)

type ListPaketDataController struct {
	ListPaketDataUsecase domain.ListPaketDataUsecase
}

func (lpdc *ListPaketDataController) GetListPaketData(c echo.Context) error {
	res, err := lpdc.ListPaketDataUsecase.GetListPaketData()
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success fetch list paket data", res)
}

// func (lpdc *ListPaketDataController) GetListPaketDataById(c echo.Context) error {
// 	id, err := strconv.Atoi(c.Param("id"))
// 	if err != nil {
// 		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
// 	}

// 	res, err := lpdc.ListPaketDataUsecase.GetListPaketDataById(id)
// 	if err != nil {
// 		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
// 	}

// 	return util.SetResponse(c, http.StatusOK, "success fetch paket data", res)
// }

func (lpdc *ListPaketDataController) CreateListPaketData(c echo.Context) error {
	var req dto.ListPaketDataDTO

	err := c.Bind(&req)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = req.Validation()
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = lpdc.ListPaketDataUsecase.CreateListPaketData(req)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusCreated, "success created paket data", nil)
}

func (lpdc *ListPaketDataController) UpdateListPaketData(c echo.Context) error {
	var req dto.ListPaketDataDTO

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = c.Bind(&req)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = req.Validation()
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = lpdc.ListPaketDataUsecase.UpdateListPaketData(id, req)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusCreated, "success update paket data", nil)

}

func (lpdc *ListPaketDataController) DeleteListPaketData(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = lpdc.ListPaketDataUsecase.DeleteListPaketData(id)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "success delete paket data", nil)
}

func (lpdc *ListPaketDataController) BeliPaketData(c echo.Context) error {
	var req dto.BeliPaketData

	err := c.Bind(&req)
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = req.ValidationBeliPaketData()
	if err != nil {
		return util.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	err = lpdc.ListPaketDataUsecase.BeliPaketData(req)
	if err != nil {
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return util.SetResponse(c, http.StatusOK, "buy paket data success", nil)
}
