package repository

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"database/sql"
)

type ListPaketDataRepository struct {
	db *sql.DB
}

func NewListPaketDataRepository(db *sql.DB) domain.ListPaketDataRepository {
	return &ListPaketDataRepository{db}
}

func (lpdr ListPaketDataRepository) GetListPaketData() ([]domain.ListPaketData, error) {
	var res []domain.ListPaketData
	sql := `SELECT * FROM list_paket_data ORDER BY id ASC`

	rows, err := lpdr.db.Query(sql)

	for rows.Next() {
		var paketData domain.ListPaketData
		err = rows.Scan(&paketData.Id, &paketData.Type, &paketData.Kuota, &paketData.Price, &paketData.Stock)
		if err != nil {
			return nil, err
		}

		res = append(res, paketData)
	}

	return res, err
}

func (lpdr ListPaketDataRepository) GetListPaketDataById(id int) (domain.ListPaketData, error) {
	var res domain.ListPaketData
	sql := `SELECT * FROM list_paket_data WHERE id = $1`

	err := lpdr.db.QueryRow(sql, id).Scan(&res.Id, &res.Type, &res.Kuota, &res.Price, &res.Stock)

	return res, err
}

func (lpdr ListPaketDataRepository) CreateListPaketData(req domain.ListPaketData) error {
	sql := `INSERT INTO list_paket_data (type, kuota, price, stock) VALUES ($1, $2, $3, $4)`

	_, err := lpdr.db.Exec(sql, req.Type, req.Kuota, req.Price, req.Stock)

	return err
}

func (lpdr ListPaketDataRepository) UpdateListPaketData(id int, req domain.ListPaketData) error {
	sql := `UPDATE list_paket_data SET type = $1, kuota = $2, price = $3, stock = $4 WHERE id = $5`

	_, err := lpdr.db.Exec(sql, req.Type, req.Kuota, req.Price, req.Stock, id)

	return err
}

func (lpdr ListPaketDataRepository) UpdateListPaketDataStock(id, stock int) error {
	sql := `UPDATE list_paket_data SET stock = $2 WHERE id = $1`

	_, err := lpdr.db.Exec(sql, id, stock)

	return err
}

func (lpdr ListPaketDataRepository) DeleteListPaketData(id int) error {
	sql := `DELETE FROM list_paket_data WHERE id = $1`

	_, err := lpdr.db.Exec(sql, id)

	return err
}
