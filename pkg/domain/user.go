package domain

import "blitzkrieg-voucher-app/pkg/dto"

type User struct {
	Id        int    `json:"id"`
	Email     string `json:"email"`
	Age       string `json:"age"`
	Password  string `json:"password"`
	CreatedAt string `json:"created_ad"`
	Username  string `json:"username"`
}

type UserView struct {
	Id        int    `json:"id"`
	Email     string `json:"email"`
	Age       string `json:"age"`
	CreatedAt string `json:"created_ad"`
	Username  string `json:"username"`
}

// UserRepository, interface yang mendefinisikan operasi-operasi terhadap data user di dalam database
type UserRepository interface {
	CreateUser(req User) error
	UpdateUser(id int, req User) error
	GetUsers() ([]UserView, error)
	GetUserById(id int) (UserView, error)
	DeleteUserById(id int) error
	FindByEmail(email string) (User, error)
	FindByUsername(username string) (User, error)
}

// UserUsecase, interface yang mendefinisikan operasi-operasi terkait user
type UserUsecase interface {
	CreateUser(req dto.UserDTO) error
	UpdateUser(id int, req dto.UserDTO) error
	GetUsers() ([]UserView, error)
	GetUserById(id int) (UserView, error)
	DeleteUserById(id int) error
	UserLogin(req dto.LoginRequest) (interface{}, error)
}
