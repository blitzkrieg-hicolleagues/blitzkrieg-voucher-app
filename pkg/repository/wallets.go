package repository

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"database/sql"
)

type WalletsRepository struct {
	db *sql.DB // instance dari koneksi database
}

func NewWalletsRepository(db *sql.DB) domain.WalletsRepository {
	return &WalletsRepository{
		db: db,
	}
}

func (wr WalletsRepository) CreateWallets(req domain.Wallets) error {
	sql := `INSERT INTO wallets (saldo, user_id) values ($1, $2)`
	_, err2 := wr.db.Exec(sql, req.Saldo, req.UserId)
	if err2 != nil {
		return err2
	}
	return nil
}

func (wr WalletsRepository) GetWallets() ([]domain.Wallets, error) {
	sql := `SELECT * FROM wallets ORDER BY id ASC`
	rows, err := wr.db.Query(sql)
	if err != nil {
		return nil, err
	}
	var wallets []domain.Wallets
	for rows.Next() {
		wallet := domain.Wallets{}
		err2 := rows.Scan(&wallet.Id, &wallet.Saldo, &wallet.UserId, &wallet.CreatedAt)
		if err2 != nil {
			return wallets, err2
		}
		wallets = append(wallets, wallet)
	}
	return wallets, err
}

func (wr WalletsRepository) GetWalletsById(id int) (domain.Wallets, error) {
	var wallet domain.Wallets
	sql := `SELECT * FROM wallets WHERE id = $1`
	err := wr.db.QueryRow(sql, id).Scan(&wallet.Id, &wallet.Saldo, &wallet.UserId, &wallet.CreatedAt)
	return wallet, err
}

func (wr WalletsRepository) UpdateWallets(id int, req domain.Wallets) error {
	sql := `UPDATE wallets SET saldo = $1, user_id = $2 WHERE id = $3`
	_, err2 := wr.db.Exec(sql, req.Saldo, req.UserId, id)
	if err2 != nil {
		return err2
	}
	return nil
}

// FindWalletByUserId, metode yang mencari daftar user berdasarkan email dari database
func (wr WalletsRepository) FindWalletByUserId(user_id int) (domain.Wallets, error) {
	var wallets domain.Wallets
	sql := `SELECT * FROM wallets WHERE user_id = $1`
	err := wr.db.QueryRow(sql, user_id).Scan(&wallets.Id, &wallets.Saldo, &wallets.UserId, &wallets.CreatedAt)
	return wallets, err
}
