# blitzkrieg-voucher-app
Voucher App | Blitzkrieg

## API Endpoints

### User Management

#### Register User

- **URL:** `/api/register`
- **Method:** `POST`
- **Body:**
    ```json
    {
        "email": "email_name@email.com",
        "username": "username",
        "age": 25,
        "password": "password"
    }
    ```

#### Login User

- **URL:** `/api/login`
- **Method:** `POST`
- **Body:**
    ```json
    {
        "email": "email_name@email.com",
        "password": "password"
    }
    ```

### Paket Management

#### Tambah Stock Paket

- **URL:** `/api/counter/stock`
- **Method:** `POST`
- **Body:**
    ```json
    {
        "type": 4,
        "kuota": 4,
        "price": 30000,
        "stock": 50
    }
    ```

#### Lihat Daftar Paket

- **URL:** `/api/counter/list`
- **Method:** `GET`

#### Beli Paket

- **URL:** `/api/counter/buy`
- **Method:** `POST`
- **Body:**
    ```json
    {
        "userId": "some_user_id",
        "listPaketDataId": "some_paket_data_id"
    }
    ```

### Support APIs

#### Get Wallet Information

- **URL:** `/api/wallet/{userId}`
- **Method:** `GET`

#### Get User Information

- **URL:** `/api/user/{userId}`
- **Method:** `GET`

#### Get Paket Data Information

- **URL:** `/api/paket-data/{userId}`
- **Method:** `GET`

#### Reset Kuota

- **URL:** `/api/paket-data/reset/{userId}`
- **Method:** `POST`

