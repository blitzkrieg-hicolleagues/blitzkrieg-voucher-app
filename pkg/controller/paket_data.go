package controller

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"blitzkrieg-voucher-app/shared/util"
	"log"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

type PaketDataController struct {
	Usecase domain.PaketDataUsecase
}

func (controller *PaketDataController) GetPaketData(c echo.Context) error {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		log.Printf("Error parsing ID: %v", err)
		return util.SetResponse(c, http.StatusBadRequest, "ID paket data harus berupa angka", nil)
	}

	paketData, err := controller.Usecase.GetPaketData(id)
	if err != nil {
		log.Printf("Error getting paket data: %v", err)
		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil) // InternalServerError jika error dari server
	}

	return util.SetResponse(c, http.StatusOK, "Berhasil mendapatkan data paket", paketData)
}

// func (controller *PaketDataController) ResetPaketData(c echo.Context) error {
// 	idStr := c.Param("id")
// 	id, err := strconv.Atoi(idStr)

// 	if err != nil {
// 		log.Printf("Error parsing ID: %v", err)
// 		return util.SetResponse(c, http.StatusBadRequest, "ID paket data harus berupa angka", nil)
// 	}
// 	if err = controller.Usecase.CreatePaketData(id); err != nil {
// 		log.Printf("Error resetting paket data: %v", err)
// 		return util.SetResponse(c, http.StatusInternalServerError, err.Error(), nil) // InternalServerError jika error dari server
// 	}

// 	return util.SetResponse(c, http.StatusOK, "Berhasil mereset paket data", nil)
// }
