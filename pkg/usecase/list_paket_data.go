package usecase

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"blitzkrieg-voucher-app/pkg/dto"
	"errors"

	"github.com/mitchellh/mapstructure"
)

type ListPaketDataUsecase struct {
	ListPaketDataRepository        domain.ListPaketDataRepository
	PaketDataRepository            domain.PaketDataRepository
	WalletsRepository              domain.WalletsRepository
	HistoryBeliPaketDataRepository domain.HistoryBeliPaketDataRepository
}

func NewListPaketDataUsecase(listPaketDataRepository domain.ListPaketDataRepository, paketDataRepository domain.PaketDataRepository, walletsRepository domain.WalletsRepository, historyBeliPaketDataRepository domain.HistoryBeliPaketDataRepository) domain.ListPaketDataUsecase {
	return &ListPaketDataUsecase{listPaketDataRepository, paketDataRepository, walletsRepository, historyBeliPaketDataRepository}
}

func (lpdu ListPaketDataUsecase) GetListPaketData() ([]domain.ListPaketData, error) {
	return lpdu.ListPaketDataRepository.GetListPaketData()
}

func (lpdu ListPaketDataUsecase) GetListPaketDataById(id int) (domain.ListPaketData, error) {
	return lpdu.ListPaketDataRepository.GetListPaketDataById(id)
}

func (lpdu ListPaketDataUsecase) CreateListPaketData(req dto.ListPaketDataDTO) error {
	var listPaketData domain.ListPaketData
	mapstructure.Decode(req, &listPaketData)

	return lpdu.ListPaketDataRepository.CreateListPaketData(listPaketData)
}

func (lpdu ListPaketDataUsecase) UpdateListPaketData(id int, req dto.ListPaketDataDTO) error {
	var listPaketData domain.ListPaketData
	mapstructure.Decode(req, &listPaketData)

	return lpdu.ListPaketDataRepository.UpdateListPaketData(id, listPaketData)
}

func (lpdu ListPaketDataUsecase) DeleteListPaketData(id int) error {
	return lpdu.ListPaketDataRepository.DeleteListPaketData(id)
}

func (lpdu ListPaketDataUsecase) BeliPaketData(req dto.BeliPaketData) error {
	resListPaketData, err := lpdu.ListPaketDataRepository.GetListPaketDataById(req.ListPaketDataId)
	if err != nil {
		return err
	}

	resPaketData, err := lpdu.PaketDataRepository.GetPaketData(req.UserId)
	if err != nil {
		return err
	}

	resWallet, err := lpdu.WalletsRepository.FindWalletByUserId(req.UserId)
	if err != nil {
		return err
	}

	if resWallet.Saldo < resListPaketData.Price {
		return errors.New("balance is not enough")
	}

	resListPaketData.Stock -= 1

	err = lpdu.ListPaketDataRepository.UpdateListPaketDataStock(req.ListPaketDataId, resListPaketData.Stock)
	if err != nil {
		return err
	}

	resWallet.Saldo -= resListPaketData.Price

	err = lpdu.WalletsRepository.UpdateWallets(resWallet.Id, resWallet)
	if err != nil {
		return err
	}

	historyBeli := domain.HistoryBeliPaketData{
		ListPaketDataId: req.ListPaketDataId,
		UserId:          req.UserId,
		PaketDataId:     resPaketData.Id,
		WalletId:        resWallet.Id,
	}

	err = lpdu.HistoryBeliPaketDataRepository.CreateHistoryBeliPaketData(historyBeli)
	if err != nil {
		return errors.New("failed create history beli paket data")
	}

	if resPaketData.Type < resListPaketData.Type {
		resPaketData.Kuota = 0
	}

	if resPaketData.Type >= resListPaketData.Type {
		resPaketData.Kuota += resListPaketData.Kuota
	}

	resPaketData.Kuota += resListPaketData.Kuota

	paketData := domain.PaketData{
		UserId: req.UserId,
		Type:   resListPaketData.Type,
		Kuota:  resPaketData.Kuota,
	}
	err = lpdu.PaketDataRepository.UpdatePaketData(paketData)
	if err != nil {
		return errors.New("failed update paket data")
	}

	return nil
}
