package util

import (
	"blitzkrieg-voucher-app/config"
	"blitzkrieg-voucher-app/pkg/domain"
	"time"

	"github.com/golang-jwt/jwt"
	"golang.org/x/crypto/bcrypt"
)

type JwtClaims struct {
	Id        int    `json:"id"`
	Email     string `json:"email"`
	Age       string `json:"age"`
	Password  string `json:"password"`
	CreatedAt string `json:"created_ad"`
	Username  string `json:"username"`
	jwt.StandardClaims
}

func CreateJwtToken(user domain.User) (string, error) {
	conf := config.GetConfig()
	claimss := JwtClaims{
		Id:        user.Id,
		Email:     user.Email,
		Age:       user.Age,
		Password:  user.Password,
		CreatedAt: user.CreatedAt,
		Username:  user.Username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
		},
	}
	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS512, claimss)
	token, err := rawToken.SignedString([]byte(conf.SignKey))
	if err != nil {
		return "", err
	}
	return token, nil
}

func EncryptPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)

	return string(bytes), err
}

func VerifyPassword(password, hash string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false, err
	}

	return true, nil
}
