package repository

import (
	"blitzkrieg-voucher-app/pkg/domain"
	"database/sql"
)

type HistoryBeliPaketDataRepository struct {
	db *sql.DB
}

func NewHistoryBeliPaketDataRepository(db *sql.DB) domain.HistoryBeliPaketDataRepository {
	return &HistoryBeliPaketDataRepository{db}
}

func (hbpdr HistoryBeliPaketDataRepository) GeHistoryBeliPaketData() ([]domain.HistoryBeliPaketData, error) {
	var res []domain.HistoryBeliPaketData
	sql := `SELECT * FROM history_beli_paket_data`

	rows, err := hbpdr.db.Query(sql)

	for rows.Next() {
		var historyBeliPaketData domain.HistoryBeliPaketData

		err := rows.Scan(&historyBeliPaketData.Id, &historyBeliPaketData.ListPaketDataId, &historyBeliPaketData.UserId, &historyBeliPaketData.PaketDataId, &historyBeliPaketData.WalletId, &historyBeliPaketData.CreatedAt)
		if err != nil {
			return nil, err
		}

		res = append(res, historyBeliPaketData)
	}

	return res, err
}

func (hbpdr HistoryBeliPaketDataRepository) GetHistoryBeliPaketDataById(id int) (domain.HistoryBeliPaketData, error) {
	var res domain.HistoryBeliPaketData
	sql := `SELECT * FROM history_beli_paket_data WHERE id = $1`

	err := hbpdr.db.QueryRow(sql, id).Scan(&res.Id, &res.ListPaketDataId, &res.UserId, &res.PaketDataId, &res.WalletId, &res.CreatedAt)

	return res, err
}

func (hbpdr HistoryBeliPaketDataRepository) CreateHistoryBeliPaketData(req domain.HistoryBeliPaketData) error {
	sql := `INSERT INTO history_beli_paket_data (list_paket_data_id, user_id, paket_data_id, wallet_id) VALUES ($1,$2, $3, $4)`

	_, err := hbpdr.db.Exec(sql, req.ListPaketDataId, req.UserId, req.PaketDataId, req.WalletId)

	return err
}

// func (hbpdr HistoryBeliPaketDataRepository) UpdateHistoryBeliPaketData(id int, req domain.HistoryBeliPaketData) error{
// }

func (hbpdr HistoryBeliPaketDataRepository) DeleteHistoryBeliPaketData(id int) error {
	sql := `DELETE FROM history_beli_paket_data WHERE id = $1`

	_, err := hbpdr.db.Exec(sql, id)

	return err
}
