package dto

import (
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
)

type HistoryBeliPaketDataDTO struct {
	Id              int       `json:"id"`
	ListPaketDataId int       `json:"list_paket_data_id"`
	UserId          int       `json:"user_id"`
	PaketDataId     int       `json:"paket_data_id"`
	WalletId        int       `json:"wallet_id"`
	CreatedAt       time.Time `json:"created_at"`
}

func (hbpd HistoryBeliPaketDataDTO) Validation() error {
	err := validation.ValidateStruct(&hbpd,
		validation.Field(&hbpd.ListPaketDataId, validation.Required),
		validation.Field(&hbpd.UserId, validation.Required),
		validation.Field(&hbpd.PaketDataId, validation.Required),
		validation.Field(&hbpd.WalletId, validation.Required),
	)

	return err
}
